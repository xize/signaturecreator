﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignatureCreator
{
    public partial class Window : Form
    {

        private Regex r = new Regex(@"[^A-Fa-f0-9?\s{1}]");

        public Window()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            //first validate.
            if(this.r.IsMatch(this.array1.Text) && this.r.IsMatch(this.array2.Text))
            {
                MessageBox.Show("you signature can only be in hex from A-F and 0-9 with ? as exception, with one spacing between other charaters.", "SignatureCreator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //end validation

            this.result.Text = Compare();
            String[] offset = this.result.Text.Split(' ');
            StringBuilder b = new StringBuilder();
            foreach(String s in offset)
            {
                b.Append("0x"+s+",");
            }
            this.style1.Text = b.ToString().Substring(0, b.Length-1);

            this.style2.Text = b.ToString().Substring(0, b.Length - 1).Replace("0x", @"\x");
        }

        private String Compare()
        {
            char[] b1 = array1.Text.ToCharArray();
            char[] b2 = array2.Text.ToCharArray();

            if(b1.Length != b2.Length)
            {
                MessageBox.Show("the aob sig need to be the same size.", "SignatureCreator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            String data = "";

            for(int i = 0; i < b1.Length; i++)
            {
                char a = b1[i];
                char b = b2[i];
                if (a == b)
                {
                    data += a;
                } else
                {
                    data += "?";
                }
            }

            return data;
        }

        private void array1_TextChanged(object sender, EventArgs e)
        {
            this.size1.Text = this.array1.Text.Length+"";
        }

        private void array2_TextChanged(object sender, EventArgs e)
        {
            this.size2.Text = this.array2.Text.Length + "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(result.Text);
            MessageBox.Show("the result has been copied!", "SignatureCreator", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.array1.Text = this.result.Text;
        }
    }
}
